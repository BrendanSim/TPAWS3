FROM maven:3.6.3-jdk-11 AS build
WORKDIR /app
COPY src ./src
COPY pom.xml .
RUN mvn clean package

FROM openjdk:11-jre-slim
WORKDIR /app
COPY --from=build /app/target/demo-isika-0.0.1-SNAPSHOT.jar /app/

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "demo-isika-0.0.1-SNAPSHOT.jar"]